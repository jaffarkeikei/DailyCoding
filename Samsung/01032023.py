# A group of houses is connected to the main water plant by means of a set of pipes. A house can either be connected
# by a set of pipes extending directly to the plant, or indirectly by a pipe to a nearby house which is otherwise
# connected.
#
# For example, here is a possible configuration, where A, B, and C are houses, and arrows represent pipes:
#
# A <--> B <--> C <--> plant Each pipe has an associated cost, which the utility company would like to minimize.
# Given an undirected graph of pipe connections, return the lowest cost configuration of pipes such that each house
# has access to water.
#
# In the following setup, for example, we can remove all but the pipes from plant to A, plant to B, and B to C,
# for a total cost of 16.
#
# pipes = {
#     'plant': {'A': 1, 'B': 5, 'C': 20},
#     'A': {'C': 15},
#     'B': {'C': 10},
#     'C': {}
# }

def find_lowest_cost_configuration(pipes):
    # Create a list of edges from the pipes dictionary
    edges = []
    for src, dest_dict in pipes.items():
        for dest, cost in dest_dict.items():
            edges.append((src, dest, cost))

    # Sort the edges in non-decreasing order of their weights
    edges.sort(key=lambda x: x[2])

    # Initialize a forest (a set of trees), where each vertex in the graph is a separate tree
    forest = {v: v for v in pipes.keys()}

    # Initialize the MST as an empty list
    mst = []

    # Iterate over the edges in the sorted list of edges
    for src, dest, cost in edges:
        # Find the trees that the vertices of the edge belong to
        src_tree = find(src, forest)
        dest_tree = find(dest, forest)

        # If the vertices belong to different trees, merge the trees and add the edge to the MST
        if src_tree != dest_tree:
            forest[src_tree] = dest_tree
            mst.append((src, dest, cost))

    # Return the MST
    return mst


def find(v, forest):
    if forest[v] == v:
        return v
    return find(forest[v], forest)


# below are test cases for the codes above

pipes = {
    'plant': {'A': 1, 'B': 5, 'C': 20},
    'A': {'C': 15},
    'B': {'C': 10},
    'C': {}
}

print(find_lowest_cost_configuration(pipes))


# def test_find_lowest_cost_configuration():
    # Test case 1
    # pipes = {
    #     'plant': {'A': 1, 'B': 5, 'C': 20},
    #     'A': {'C': 15},
    #     'B': {'C': 10},
    #     'C': {}
    # }
    # assert find_lowest_cost_configuration(pipes) == [('plant', 'A', 1), ('plant', 'B', 5), ('B', 'C', 10)]

    # Test case 2
    # pipes = {
    #     'plant': {'A': 1, 'B': 5, 'C': 20},
    #     'A': {'C': 15},
    #     'B': {'C': 10},
    #     'C': {'D': 5},
    #     'D': {}
    # }
    # assert find_lowest_cost_configuration(pipes) == [('plant', 'A', 1), ('plant', 'B', 5), ('B', 'C', 10),
    #                                                  ('C', 'D', 5)]
    #
    # # Test case 3
    # pipes = {
    #     'plant': {'A': 1, 'B': 5, 'C': 20},
    #     'A': {'B': 15},
    #     'B': {'C': 10},
    #     'C': {}
    # }
    # assert find_lowest_cost_configuration(pipes) == [('plant', 'A', 1), ('A', 'B', 15), ('B', 'C', 10)]
    #
    # print('All test cases pass')

