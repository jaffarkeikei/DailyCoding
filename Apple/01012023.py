# Given a tree, find and return the size of the largest tree/subtree that is a BST.

class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def is_bst(root):
    # Base case: if the tree is empty, it is a BST
    if root is None:
        return True

    # If the tree is a BST, recursively check the left and right subtrees
    if (root.left is None or root.left.val < root.val) and (root.right is None or root.right.val > root.val):
        return is_bst(root.left) and is_bst(root.right)

    # If the tree is not a BST, return False
    return False


def find_largest_bst(root):
    # Base case: if the tree is empty, return 0
    if root is None:
        return 0

    # If the tree is a BST, return the size of the tree
    if is_bst(root):
        return 1 + find_largest_bst(root.left) + find_largest_bst(root.right)

    # If the tree is not a BST, return the maximum of the sizes of the left and right subtrees
    return max(find_largest_bst(root.left), find_largest_bst(root.right))


def largest_bst(root):
    return find_largest_bst(root)

