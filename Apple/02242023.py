# To implement the function fib(n) using O(1) space,
# we can use a technique called "loop unrolling".
# This involves keeping track of only the two previous
# numbers in the sequence and updating them as we
# iterate through the loop. Here's how it works:
def fib(n):
    if n < 2:
        return n

    a, b = 0, 1

    for _ in range(2, n + 1):
        c = a + b
        a, b = b, c

    return b
