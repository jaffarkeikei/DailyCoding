# This problem was aThis problem was asked by Yahoo.
#
# Recall that a full binary tree is one in which each node is either a leaf node, or has two children. Given a binary tree, convert it to a full one by removing nodes with only one child.
#
# For example, given the following tree:
#
#          0
#       /     \
#     1         2
#   /            \
# 3                 4
#   \             /   \
#     5          6     7
# You should convert it to:
#
#      0
#   /     \
# 5         4
#         /   \
       # 6     7

class Node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

def remove_nodes_with_one_child(root):
    if not root:
        return None
    root.left = remove_nodes_with_one_child(root.left)
    root.right = remove_nodes_with_one_child(root.right)
    if not root.left:
        return root.right
    if not root.right:
        return root.left
    return root

# Example usage:
# Constructing the example tree
root = Node(0)
root.left = Node(1)
root.left.left = Node(3)
root.left.left.right = Node(5)
root.right = Node(2)
root.right.right = Node(4)
root.right.right.left = Node(6)
root.right.right.right = Node(7)

# Converting the tree to a full binary tree
new_root = remove_nodes_with_one_child(root)

# Printing the new tree
print(new_root.val)
print(new_root.left.val, new_root.right.val)
print(new_root.right.left.val, new_root.right.right.val)
