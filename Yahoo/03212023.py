# Write an algorithm that computes the reversal of a directed graph.
# For example, if a graph consists of A -> B -> C, it should become A <- B <- C.

def reverse_graph(graph):
    new_graph = {}
    for vertex in graph:
        new_graph[vertex] = []
    for vertex in graph:
        for neighbor in graph[vertex]:
            new_graph[neighbor].append(vertex)
    return new_graph
