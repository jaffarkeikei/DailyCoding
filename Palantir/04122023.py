# A typical American-style crossword puzzle grid is an N x N matrix with black and white squares, which obeys the following rules:
#
# Every white square must be part of an "across" word and a "down" word.
# No word can be fewer than three letters long.
# Every white square must be reachable from every other white square.
# The grid is rotationally symmetric (for example, the colors of the top left and bottom right squares must match).
# Write a program to determine whether a given matrix qualifies as a crossword grid.

def is_crossword_grid(grid):
    n = len(grid)

    # Check if every white square is part of both an "across" word and a "down" word.
    if not all(grid[i][j] == "B" or \
               (j > 0 and grid[i][j - 1] == "W") or \
               (j < n - 1 and grid[i][j + 1] == "W") or \
               (i > 0 and grid[i - 1][j] == "W") or \
               (i < n - 1 and grid[i + 1][j] == "W") \
               for i in range(n) for j in range(n) if grid[i][j] == "W"):
        return False

    # Check if there are any words with less than three letters.
    if any(sum(grid[i][j] == "W" for j in range(k, n) if grid[i][j] == "W") < 3 for i in range(n) for k in
           range(n - 2)):
        return False

    # Perform a graph traversal and check if every other white square is reachable from the starting square.
    def dfs(i, j, visited):
        if i < 0 or i >= n or j < 0 or j >= n or grid[i][j] == "B" or visited[i][j]:
            return
        visited[i][j] = True
        dfs(i - 1, j, visited)
        dfs(i + 1, j, visited)
        dfs(i, j - 1, visited)
        dfs(i, j + 1, visited)

    # Find the starting square for the graph traversal
    start_i, start_j = next((i, j) for i in range(n) for j in range(n) if grid[i][j] == "W")

    # Perform the graph traversal and check if every white square is reachable from the starting square
    visited = [[False] * n for _ in range(n)]
    dfs(start_i, start_j, visited)
    if not all(visited[i][j] for i in range(n) for j in range(n) if grid[i][j] == "W"):
        return False

    # Check if the grid is rotationally symmetric
    if any(grid[i][j] != grid[n - 1 - i][n - 1 - j] or grid[i][j] != grid[j][i] or grid[i][j] != grid[n - 1 - j][
        n - 1 - i] for i in range(n) for j in range(n)):
        return False

    # If all conditions are satisfied, return True
    return True

