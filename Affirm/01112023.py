# Given a array of numbers representing the stock prices of a company in chronological order, write a function that
# calculates the maximum profit you could have made from buying and selling that stock. You're also given a number
# fee that represents a transaction fee for each buy and sell transaction.
#
# You must buy before you can sell the stock, but you can make as many transactions as you like.
#
# For example, given [1, 3, 2, 8, 4, 10] and fee = 2, you should return 9, since you could buy the stock at 1 dollar,
# and sell at 8 dollars, and then buy it at 4 dollars and sell it at 10 dollars. Since we did two transactions,
# there is a 4 dollar fee, so we have 7 + 6 = 13 profit minus 4 dollars of fees.

def maxProfit(prices, fee):
    n = len(prices)
    dp_0, dp_1 = 0, -prices[0]  # dp_0 = max profit when not holding stock, dp_1 = max profit when holding stock
    for i in range(1, n):
        temp = dp_0
        dp_0 = max(dp_0, dp_1 + prices[i] - fee)  # if not holding stock, then either hold previous max_profit or
        # sell current stock
        dp_1 = max(dp_1, temp - prices[i])  # if holding stock, either hold previous max_profit or buy current stock
    return dp_0


prices = [1, 3, 2, 8, 4, 10]
fee = 2
print(maxProfit(prices, fee))  # should return 9


# better solution
def maxProfits(prices, fee):
    dp_0, dp_1 = 0, -prices[0]
    for i in range(1, len(prices)):
        dp_0, dp_1 = max(dp_0, dp_1 + prices[i] - fee), max(dp_1, dp_0 - prices[i])
    return dp_0


prices = [1, 3, 2, 8, 4, 10]
fee = 2
print(maxProfits(prices, fee))  # should return 9
