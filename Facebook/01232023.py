# Given an array of integers in which two elements appear exactly once
# and all other elements appear exactly twice, find the two elements that appear only once.
#
# For example, given the array [2, 4, 6, 8, 10, 2, 6, 10], return 4 and 8. The order does not matter.
#
# Follow-up: Can you do this in linear time and constant space?

def find_single_numbers(arr):
    xor = 0
    for num in arr:
        xor ^= num
    mask = xor & (-xor)
    a = b = 0
    for num in arr:
        if num & mask:
            a ^= num
        else:
            b ^= num
    return [a, b]


