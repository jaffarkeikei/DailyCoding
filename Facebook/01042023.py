# Given an array of numbers representing the stock prices of a company in chronological order and an integer k,
# return the maximum profit you can make from k buys and sells. You must buy the stock before you can sell it,
# and you must sell the stock before you can buy it again.
#
# For example, given k = 2 and the array [5, 2, 4, 0, 1], you should return 3.

def max_profit(prices, k):
    # Initialize the maximum profit to 0
    max_profit = 0

    # Repeat the following steps k times
    for _ in range(k):
        # Initialize the minimum price and the current profit to 0
        min_price = float('inf')
        profit = 0

        # Iterate over the array of stock prices
        for price in prices:
            # Update the minimum price
            min_price = min(min_price, price)

            # Update the maximum profit
            profit = max(profit, price - min_price)

        # Add the current profit to the total maximum profit
        max_profit += profit

    # Return the maximum profit
    return max_profit

