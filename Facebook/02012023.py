# Given an integer n, find the next biggest integer with the same number
# of 1-bits on. For example, given the number 6 (0110 in binary), return 9 (1001).

def next_biggest_integer(n):
    # Convert the integer to its binary representation
    binary = bin(n)[2:]

    # Find the rightmost non-trailing zero
    for i in range(len(binary) - 1, 0, -1):
        if binary[i] == '0' and binary[i - 1] == '1':
            break

    # Flip the rightmost non-trailing zero
    binary = binary[:i] + '1' + '0' * (len(binary) - i - 1)

    # Increment the rightmost bits to the next largest number with the same number of 1-bits
    for i in range(len(binary) - 1, 0, -1):
        if binary[i] == '0':
            binary = binary[:i] + '1' + binary[i + 1:]
            break

    # Convert the binary string back to an integer and return it
    return int(binary, 2)
