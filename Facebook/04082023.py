# Given a stream of elements too large to store in memory,
# pick a random element from the stream with uniform probability.

import random

def reservoir_sampling(stream, k):
    reservoir = []  # Initialize an empty reservoir list of size k

    i = 0  # Initialize a counter to keep track of the number of elements seen so far
    for e in stream:
        i += 1
        if len(reservoir) < k:  # If the length of the reservoir is less than k, add e to the reservoir
            reservoir.append(e)
        else:  # Otherwise, replace an element in the reservoir with e with probability 1/i
            j = random.randint(0, i-1)  # Generate a random index j between 0 and i-1
            if j < k:
                reservoir[j] = e
    return random.choice(reservoir)  # Return a random element from the reservoir
