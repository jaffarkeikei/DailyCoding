# Given a circular array, compute its maximum subarray sum in O(n) time. A subarray can be empty, and in this case the sum is 0.
#
# For example, given [8, -1, 3, 4], return 15 as we choose the numbers 3, 4, and 8 where the 8 is obtained from wrapping around.
#
# Given [-4, 5, 1, 0], return 6 as we choose the numbers 5 and 1.

def max_subarray_sum_circular(arr):
    n = len(arr)
    max_sum = curr_sum = arr[0]
    for i in range(1, n):
        curr_sum = max(arr[i], curr_sum + arr[i])
        max_sum = max(max_sum, curr_sum)
    curr_sum = 0
    total_sum = sum(arr)
    for i in range(1, n):
        curr_sum = min(arr[i], curr_sum + arr[i])
        max_sum = max(max_sum, total_sum - curr_sum)
    return max_sum
