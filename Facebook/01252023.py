# Given a binary tree, return the level of the tree with minimum sum.

from collections import deque


class Node:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


def minimum_sum_level(root):
    if not root:
        return None

    level_sums = []
    queue = deque()
    queue.append(root)
    while queue:
        level_sum = 0
        level_size = len(queue)
        for i in range(level_size):
            current = queue.popleft()
            level_sum += current.val
            if current.left:
                queue.append(current.left)
            if current.right:
                queue.append(current.right)
        level_sums.append(level_sum)

    min_sum = float('inf')
    min_sum_level = None
    for i in range(len(level_sums)):
        if level_sums[i] < min_sum:
            min_sum = level_sums[i]
            min_sum_level = i

    return min_sum_level
