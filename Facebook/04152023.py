# Given a function that generates perfectly random numbers between 1 and k
# (inclusive), where k is an input, write a function that shuffles
# a deck of cards represented as an array using only swaps.
#
# It should run in O(N) time.
#
# Hint: Make sure each one of the 52! permutations of the deck is equally likely.

import random

def shuffle_deck(deck):
    n = len(deck)
    for i in range(n):
        # Generate a random index
        r = random.randint(i, n-1)
        # Swap the current card with the randomly selected card
        deck[i], deck[r] = deck[r], deck[i]
