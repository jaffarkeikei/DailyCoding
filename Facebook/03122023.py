# Given an N by N matrix, rotate it by 90 degrees clockwise.
#
# For example, given the following matrix:
#
# [[1, 2, 3],
#  [4, 5, 6],
#  [7, 8, 9]]
# you should return:
#
# [[7, 4, 1],
#  [8, 5, 2],
#  [9, 6, 3]]

def rotateMatrix(matrix):
    N = len(matrix)
    for i in range(N // 2):
        for j in range(i, N - i - 1):
            temp = matrix[i][j]
            matrix[i][j] = matrix[N - 1 - j][i]
            matrix[N - 1 - j][i] = matrix[N - 1 - i][N - 1 - j]
            matrix[N - 1 - i][N - 1 - j] = matrix[j][N - 1 - i]
            matrix[j][N - 1 - i] = temp
    return matrix
