# Given a string of parentheses, find the balanced string that

# For example, given "(()", you could return "(())". Given "))()(",
# you could return "()()()()".

def balance_parentheses(s):
    stack = []
    result = []
    for char in s:
        if char == '(':
            stack.append(char)
        elif char == ')':
            if stack:
                stack.pop()
            else:
                result.append('(')
    result.extend([')'] * len(stack))
    return ''.join(result)
