# Write a program to merge two binary trees. Each node in the new tree should
# hold a value equal to the sum of the values of the corresponding nodes of the input trees.
#
# If only one input tree has a node in a given position, the corresponding node in
# the new tree should match that input node.

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def mergeTrees(t1: TreeNode, t2: TreeNode) -> TreeNode:
    if not t1:
        return t2
    if not t2:
        return t1
    stack = [(t1, t2)]
    while stack:
        n1, n2 = stack.pop()
        if n1 and n2:
            n1.val += n2.val
            if not n1.left:
                n1.left = n2.left
            else:
                stack.append((n1.left, n2.left))
            if not n1.right:
                n1.right = n2.right
            else:
                stack.append((n1.right, n2.right))
    return t1
