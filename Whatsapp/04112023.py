# Given an array of integers out of order, determine
# the bounds of the smallest window that must be sorted in
# order for the entire array to be sorted. For example,
# given [3, 7, 5, 6, 9], you should return (1, 3).

def smallest_window(arr):
    left = None
    right = None
    n = len(arr)

    # Find the first element from the left that is greater than the next element.
    for i in range(n - 1):
        if arr[i] > arr[i + 1]:
            left = i
            break

    # If the array is already sorted, return (0, -1).
    if left is None:
        return (0, -1)

    # Find the first element from the right that is less than the previous element.
    for i in range(n - 1, 0, -1):
        if arr[i] < arr[i - 1]:
            right = i
            break

    # Determine the bounds of the smallest window that must be sorted.
    min_val = min(arr[left:right + 1])
    max_val = max(arr[left:right + 1])
    while left > 0 and arr[left - 1] > min_val:
        left -= 1
    while right < n - 1 and arr[right + 1] < max_val:
        right += 1

    return (left, right)

