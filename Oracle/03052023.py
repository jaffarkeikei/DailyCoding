# We say a number is sparse if there are no adjacent ones in
# its binary representation. For example, 21 (10101) is sparse,
# but 22 (10110) is not. For a given input N, find the smallest
# sparse number greater than or equal to N.
#
# Do this in faster than O(N log N) time.

def nextSparseNumber(N):
    binary = bin(N)[2:]
    length = len(binary)
    for i in range(length - 1):
        if binary[i] == '1' and binary[i + 1] == '1':
            break
    else:
        return N

    for i in range(i + 1, length):
        if binary[i] == '0':
            binary = binary[:i] + '1' + '0' * (length - i - 1)
            break

    return int(binary, 2)
