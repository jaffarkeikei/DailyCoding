# Given a binary search tree, find the floor and ceiling of a given integer.
# The floor is the highest element in the tree less than or equal to an integer,
# while the ceiling is the lowest element in the tree greater than or equal to an integer.
#
# If either value does not exist, return None.

class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

def find_floor_ceiling(root, x):
    floor = None
    ceiling = None
    while root:
        if root.val == x:
            return (root.val, root.val)
        elif root.val < x:
            floor = root.val
            root = root.right
        else:
            ceiling = root.val
            root = root.left
    return (floor, ceiling)
