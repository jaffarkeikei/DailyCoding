# You are presented with an 8 by 8 matrix representing the positions of pieces on a chess board.
# The only pieces on the board are the black king and various white pieces. Given this matrix,
# determine whether the king is in check.
#
# For details on how each piece moves, see here.
#
# For example, given the following matrix:
#
# ...K....
# ........
# .B......
# ......P.
# .......R
# ..N.....
# ........
# .....Q..
# You should return True, since the bishop is attacking the king diagonally.
def is_king_in_check(board):
    # Find the position of the black king
    for i in range(8):
        for j in range(8):
            if board[i][j] == 'K':
                king_x, king_y = i, j

    # Iterate through each white piece on the board
    for i in range(8):
        for j in range(8):
            if board[i][j] != 'K' and board[i][j].isupper():
                # Check if the white piece can attack the black king's position
                if can_attack(board[i][j], (i, j), (king_x, king_y)):
                    return True

    return False


# Helper function to check if a piece can attack a certain position
def can_attack(piece, piece_pos, target_pos):
    # Write the logic for each piece here
    if piece == 'P':
        return (piece_pos[0] - 1 == target_pos[0] and abs(piece_pos[1] - target_pos[1]) == 1)
    elif piece == 'R':
        return (piece_pos[0] == target_pos[0] or piece_pos[1] == target_pos[1])
    elif piece == 'N':
        return (abs(piece_pos[0] - target_pos[0]) == 2 and abs(piece_pos[1] - target_pos[1]) == 1) or (
                    abs(piece_pos[0] - target_pos[0]) == 1 and abs(piece_pos[1] - target_pos[1]) == 2)
    elif piece == 'B':
        return abs(piece_pos[0] - target_pos[0]) == abs(piece_pos[1] - target_pos[1])
    elif piece == 'Q':
        return (piece_pos[0] == target_pos[0] or piece_pos[1] == target_pos[1]) or abs(
            piece_pos[0] - target_pos[0]) == abs(piece_pos[1] - target_pos[1])
    elif piece == 'K':
        return abs(piece_pos[0] - target_pos[0]) <= 1 and abs(piece_pos[1] - target_pos[1]) <= 1
    else:
        return False
