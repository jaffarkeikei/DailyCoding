# Given a binary tree, determine if it is height-balanced. A height-balanced binary tree can be defined
# as one in which the heights of the two subtrees of any node never differ by more than one.

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


def is_height_balanced(root: TreeNode) -> bool:
    if root is None:
        return True

    left_height = height(root.left)
    right_height = height(root.right)

    if abs(left_height - right_height) > 1:
        return False

    return is_height_balanced(root.left) and is_height_balanced(root.right)


def height(node: TreeNode) -> int:
    if node is None:
        return 0

    left_height = height(node.left)
    right_height = height(node.right)

    return max(left_height, right_height) + 1
