# Given a string and a set of characters, return the shortest substring
# containing all the characters in the set.
#
# For example, given the string "figehaeci" and the set of characters
# {a, e, i}, you should return "aeci".
#
# If there is no substring containing all the characters in the set, return null.


def shortest_substring(s, chars):
    char_count = len(chars)
    char_map = {}
    for c in chars:
        char_map[c] = 0

    min_len = float('inf')
    min_start = 0
    count = 0
    start = 0
    for i in range(len(s)):
        if s[i] in char_map:
            char_map[s[i]] += 1
            if char_map[s[i]] == 1:
                count += 1

        while count == char_count:
            if i - start + 1 < min_len:
                min_len = i - start + 1
                min_start = start
            if s[start] in char_map:
                char_map[s[start]] -= 1
                if char_map[s[start]] == 0:
                    count -= 1
            start += 1

    if min_len != float('inf'):
        return s[min_start:min_start + min_len]
    else:
        return None
