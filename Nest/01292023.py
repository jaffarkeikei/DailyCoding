# This problem was asked by Nest.
#
# Create a basic sentence checker that takes in a stream of characters and
# determines whether they form valid sentences. If a sentence is valid, the program
# should print it out.
#
# We can consider a sentence valid if it conforms to the following rules:
#
# The sentence must start with a capital letter, followed by a lowercase letter or a space.
# All other characters must be lowercase letters, separators (,,;,:) or terminal marks (.,?,!,‽).
# There must be a single space between each word.
# The sentence must end with a terminal mark immediately following a word.

import re

def check_sentence(sentence):
    # Check if the sentence starts with a capital letter and ends with a terminal mark
    if not re.match("^[A-Z][a-z ]*[.?!‽]$", sentence):
        return False
    # Check if there is a single space between each word
    if re.search("[a-z][^a-z ]", sentence):
        return False
    return True

sentence = input("Enter a sentence: ")
if check_sentence(sentence):
    print("Valid sentence:", sentence)
else:
    print("Invalid sentence")
