# Given an even number (greater than 2), return two prime
# numbers whose sum will be equal to the given number.

import math

def is_prime(n):
    if n <= 1:
        return False
    for i in range(2, int(math.sqrt(n)) + 1):
        if n % i == 0:
            return False
    return True

def get_primes_summing_to_even_number(n):
    primes = [x for x in range(2, n) if is_prime(x)]
    for p in primes:
        if is_prime(n - p):
            return p, n - p
    return None
