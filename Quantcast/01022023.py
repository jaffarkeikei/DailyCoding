# You are presented with an array representing a Boolean expression. The elements are of two kinds:
#
# T and F, representing the values True and False. &, |, and ^, representing the bitwise operators for AND, OR,
# and XOR. Determine the number of ways to group the array elements using parentheses so that the entire expression
# evaluates to True.
#
# For example, suppose the input is ['F', '|', 'T', '&', 'T']. In this case, there are two acceptable groupings: (F |
# T) & T and F | (T & T).
def count_true_groupings(arr):
    # Base case: if the array is empty, there are no acceptable groupings
    if not arr:
        return 0

    # Initialize a counter to keep track of the number of acceptable groupings
    count = 0

    # Iterate over the elements in the array
    for i in range(len(arr)):
        # If the element is a boolean value, increment the counter
        if arr[i] in ['T', 'F']:
            count += 1
        # If the element is a bitwise operator, consider the number of acceptable groupings on either side of the
        # operator
        else:
            count += count_true_groupings(arr[:i]) * count_true_groupings(arr[i + 1:])

    return count


def count_true_groupingsS(arr):
    # Initialize a counter to keep track of the number of acceptable groupings
    count = 0

    # Create a stack to keep track of the elements and their corresponding indices in the array
    stack = []

    # Iterate over the elements in the array
    for i, elem in enumerate(arr):
        # If the element is a boolean value, push its index onto the stack
        if elem in ['T', 'F']:
            stack.append(i)
        # If the element is a bitwise operator, pop the top two indices from the stack and consider the number of
        # acceptable groupings for the corresponding subarrays
        else:
            right = stack.pop()
            left = stack.pop()
            count += count_true_groupingsS(arr[left:right + 1])

    return count
