# Given a string which we can delete at most k, return whether you can make a palindrome.
#
# For example, given 'waterrfetawx' and a k of 2, you could delete f and x to get 'waterretaw'.

def can_make_palindrome(string, k):
    # Initialize the count variable to 0
    count = 0

    # Iterate over the characters in the string
    for i in range(len(string) // 2):
        # Check if the character at index i is equal to its corresponding character in the reverse of the string
        if string[i] != string[-i - 1]:
            # If it is not equal, increment count by 1
            count += 1

    # Return True if count is less than or equal to k, and False otherwise
    return count <= k


# Test the can_make_palindrome function
string = 'waterrfetawx'
k = 2
print(can_make_palindrome(string, k))  # should print True

