# Given a set of distinct positive integers,
# find the largest subset such that every pair
# of elements in the subset (i, j) satisfies either
# i % j = 0 or j % i = 0.
#
# For example, given the set [3, 5, 10, 20, 21],
# you should return [5, 10, 20]. Given [1, 3, 6, 24],
# return [1, 3, 6, 24].

def largest_subset(nums):
    nums.sort(reverse=True)
    dp = [1] * len(nums)
    for i in range(len(nums)):
        for j in range(i):
            if nums[i] % nums[j] == 0:
                dp[i] = max(dp[i], dp[j] + 1)
    max_len = max(dp)
    result = []
    for i in range(len(nums) - 1, -1, -1):
        if dp[i] == max_len and (not result or nums[i] <= result[-1] * result[-1]):
            result.append(nums[i])
            max_len -= 1
    return result[::-1]
