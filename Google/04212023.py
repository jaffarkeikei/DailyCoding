# Given a linked list, sort it in O(n log n) time and constant space.
#
# For example, the linked list 4 -> 1 -> -3 -> 99 should become -3 -> 1 -> 4 -> 99.

class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def sortList(head: ListNode) -> ListNode:
    # Base case: If the linked list has zero or one element, it is already sorted.
    if not head or not head.next:
        return head

    # Divide the linked list into two halves using the fast-slow pointer technique.
    slow = head
    fast = head.next
    while fast and fast.next:
        slow = slow.next
        fast = fast.next.next

    # Sort the two halves recursively using Merge Sort.
    second_half = slow.next
    slow.next = None
    first_half_sorted = sortList(head)
    second_half_sorted = sortList(second_half)

    # Merge the two sorted halves back together.
    return merge(first_half_sorted, second_half_sorted)


def merge(first: ListNode, second: ListNode) -> ListNode:
    dummy = ListNode()
    tail = dummy

    while first and second:
        if first.val <= second.val:
            tail.next = first
            first = first.next
        else:
            tail.next = second
            second = second.next
        tail = tail.next

    # Append the remaining nodes from either of the two linked lists to the end of the merged list.
    if first:
        tail.next = first
    else:
        tail.next = second

    return dummy.next
