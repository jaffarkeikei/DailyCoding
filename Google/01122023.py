# You are in an infinite 2D grid where you can move in any of the 8 directions:
#
# (x,y) to (x+1, y), (x - 1, y), (x, y+1), (x, y-1), (x-1, y-1), (x+1,y+1), (x-1,y+1), (x+1,y-1) You are given a
# sequence of points and the order in which you need to cover the points. Give the minimum number of steps in which
# you can achieve it. You start from the first point.
#
# Example:
#
# Input: [(0, 0), (1, 1), (1, 2)]
# Output: 2
# It takes 1 step to move from (0, 0) to (1, 1). It takes one more step to move from (1, 1) to (1, 2).

def minSteps(points):
    # Initialize 2D array with large value
    INF = float('inf')
    n = len(points)
    dp = [[INF for j in range(n)] for i in range(n)]
    dp[0][0] = 0

    # Iterate through the sequence of points
    for i in range(1, n):
        x, y = points[i]
        for j in range(x - 1, x + 2):
            for k in range(y - 1, y + 2):
                if 0 <= j < n and 0 <= k < n:
                    dp[j][k] = min(dp[j][k], dp[x][y] + 1)

    return dp[x][y]


# Example usage
points = [(0, 0), (1, 1), (1, 2)]
print(minSteps(points))  # Output: 2

