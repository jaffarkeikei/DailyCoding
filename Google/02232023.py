# You're given a string consisting solely of (, ), and *. * can
# represent either a (, ), or an empty string. Determine whether
# the parentheses are balanced.
#
# For example, (()* and (*) are balanced. )*( is not balanced.

def is_balanced(s):
    stack = []
    for c in s:
        if c == '(':
            stack.append(c)
        elif c == ')':
            if stack and stack[-1] == '(':
                stack.pop()
            else:
                return False
        elif c == '*':
            # consider * as an opening parenthesis
            stack1 = stack + ['(']
            if is_balanced(s[1:], stack1):
                return True
            # consider * as a closing parenthesis
            stack2 = stack[:-1]
            if is_balanced(s[1:], stack2):
                return True
            # consider * as an empty string
            if is_balanced(s[1:], stack):
                return True
            return False
    return not stack


# def is_balanced(s):
#     min_count = max_count = 0
#     for c in s:
#         if c == '(':
#             min_count += 1
#             max_count += 1
#         elif c == ')':
#             min_count = max(min_count - 1, 0)
#             max_count -= 1
#         elif c == '*':
#             min_count = max(min_count - 1, 0)
#             max_count += 1
#         if max_count < 0:
#             return False
#     return min_count == 0
