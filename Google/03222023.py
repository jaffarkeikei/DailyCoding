# Find the minimum number of coins required to make n cents.
#
# You can use standard American denominations, that is, 1¢, 5¢, 10¢, and 25¢.
#
# For example, given n = 16, return 3 since we can make it with a 10¢, a 5¢, and a 1¢.

def min_coins(n):
    dp = [float('inf')] * (n + 1)
    dp[0] = 0
    for i in range(1, n + 1):
        if i >= 1:
            dp[i] = min(dp[i], dp[i-1] + 1)
        if i >= 5:
            dp[i] = min(dp[i], dp[i-5] + 1)
        if i >= 10:
            dp[i] = min(dp[i], dp[i-10] + 1)
        if i >= 25:
            dp[i] = min(dp[i], dp[i-25] + 1)
    return dp[n]
