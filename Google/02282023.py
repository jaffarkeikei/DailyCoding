# Given a word W and a string S, find all starting indices
# in S which are anagrams of W.
#
# For example, given that W is "ab", and S is "abxaba",
# return 0, 3, and 4.
#
def findAnagrams(W, S):
    result = []
    freq = [0] * 26
    for c in W:
        freq[ord(c) - ord('a')] += 1

    for i in range(len(S) - len(W) + 1):
        window = freq.copy()
        for j in range(i, i + len(W)):
            idx = ord(S[j]) - ord('a')
            if window[idx] <= 0:
                break
            window[idx] -= 1
        else:
            result.append(i)

    return result
