# Given the root of a binary search tree, and a target K, return two
# nodes in the tree whose sum equals K.
#
# For example, given the following tree and K of 20
#
#     10
#    /   \
#  5      15
#        /  \
#      11    15
# Return the nodes 5 and 15.

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def find_target(root, K):
    def inorder(node):
        if node:
            inorder(node.left)
            node_vals.append(node.val)
            inorder(node.right)

    node_vals = []
    inorder(root)
    i, j = 0, len(node_vals) - 1
    while j > i:
        if node_vals[i] + node_vals[j] == K:
            return (node_vals[i], node_vals[j])
        elif node_vals[i] + node_vals[j] < K:
            i += 1
        else:
            j -= 1
    return None
