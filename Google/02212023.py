# Given an array of strictly the characters 'R', 'G', and 'B',
# segregate the values of the array so that all the Rs come first,
# the Gs come second, and the Bs come last. You can only swap elements of the array.
#
# Do this in linear time and in-place.
#
# For example, given the array ['G', 'B', 'R', 'R', 'B', 'R', 'G'],
# it should become ['R', 'R', 'R', 'G', 'G', 'B', 'B'].

def sort_rgb(arr):
    r, g, b = 0, 0, len(arr)-1
    while g <= b:
        if arr[g] == 'R':
            arr[r], arr[g] = arr[g], arr[r]
            r += 1
            g += 1
        elif arr[g] == 'G':
            g += 1
        else:
            arr[g], arr[b] = arr[b], arr[g]
            b -= 1
    return arr

