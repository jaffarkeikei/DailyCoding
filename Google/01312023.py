# Design a system to crawl and copy all of Wikipedia using a distributed network of machines.
#
# More specifically, suppose your server has access to a set of client machines.
# Your client machines can execute code you have written to access Wikipedia pages,
# download and parse their data, and write the results to a database.
#
# Some questions you may want to consider as part of your solution are:
#
# How will you reach as many pages as possible?
# How can you keep track of pages that have already been visited?
# How will you deal with your client machines being blacklisted?
# How can you update your database when Wikipedia pages are added or updated?

# Develop a program to access and parse Wikipedia pages: A program that can access and parse Wikipedia pages, extract relevant data and save it to a database needs to be developed.
# Set up a distributed network of client machines: The client machines will run the program developed in step 1 and access Wikipedia pages in parallel.
# Track pages visited: To avoid revisiting already accessed pages, a mechanism to keep track of visited pages, such as a hash table, needs to be implemented.
# Dealing with blacklisted client machines: If a client machine is blacklisted, it can be taken out of the network and replaced with a new machine. To prevent future blacklisting, the program can be updated to use different IP addresses or user agents when accessing Wikipedia.
# Updating the database: The system can be set up to periodically check for updates to Wikipedia pages and update the corresponding entries in the database accordingly.

# import wikipediaapi
#
# wiki = wikipediaapi.Wikipedia(
#         language='en',
#         extract_format=wikipediaapi.ExtractFormat.WIKI
# )
#
# page = wiki.page('Python (programming language)')
#
# print("Page - Summary: %s" % page.summary[0:60])
# print("Page - Exists: %s" % page.exists())
# print("Page - Text: %s" % page.text[0:60])
