# You are given an M by N matrix consisting
# of booleans that represents a board. Each True boolean represents a wall.
# Each False boolean represents a tile you can walk on.
#
# Given this matrix, a start coordinate,
# and an end coordinate, return the minimum number of steps required to reach
# the end coordinate from the start. If there is no possible path, then return null.
# You can move up, left, down, and right. You cannot move through walls.
# You cannot wrap around the edges of the board.
#
# For example, given the following board:
#
# [[f, f, f, f],
# [t, t, f, t],
# [f, f, f, f],
# [f, f, f, f]]
# and start = (3, 0) (bottom left) and end = (0, 0) (top left),
# the minimum number of steps required to reach the
# end is 7, since we would need to go through (1, 2) because there
# is a wall everywhere else on the second row.

from collections import deque


def min_steps(board, start, end):
    m, n = len(board), len(board[0])
    queue = deque([(start[0], start[1], 0)])
    visited = set([(start[0], start[1])])
    directions = [(0, 1), (0, -1), (1, 0), (-1, 0)]

    while queue:
        x, y, steps = queue.popleft()
        if (x, y) == end:
            return steps
        for dx, dy in directions:
            nx, ny = x + dx, y + dy
            if 0 <= nx < m and 0 <= ny < n and board[nx][ny] == False and (nx, ny) not in visited:
                queue.append((nx, ny, steps + 1))
                visited.add((nx, ny))

    return None
