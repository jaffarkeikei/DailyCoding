# PageRank is an algorithm used by Google to rank the importance of different websites. While there have been changes
# over the years, the central idea is to assign each site a score based on the importance of other pages that link to
# that page.
#
# More mathematically, suppose there are N sites, and each site i has a certain count Ci of outgoing links. Then the
# score for a particular site Sj is defined as :
#
# score(Sj) = (1 - d) / N + d * (score(Sx) / Cx+ score(Sy) / Cy+ ... + score(Sz) / Cz))
#
# Here, Sx, Sy, ..., Sz denote the scores of all the other sites that have outgoing links to Sj, and d is a damping
# factor, usually set to around 0.85, used to model the probability that a user will stop searching.
#
# Given a directed graph of links between various websites, write a program that calculates each site's page rank.

def page_rank(graph, d):
    # Initialize the scores for all the sites to 1 / N, where N is the total number of sites in the graph
    scores = {site: 1 / len(graph) for site in graph.keys()}

    # Iterate over the sites in the graph
    for site, links in graph.items():
        # Calculate the score for the site using the formula given in the prompt
        score = (1 - d) / len(graph)
        for dest, count in links.items():
            score += d * scores[dest] / count
        # Update the score for the site in the scores dictionary
        scores[site] = score

    # Return the scores dictionary
    return scores


# Test the page_rank function
graph = {
    'A': {'B': 1, 'C': 1},
    'B': {'A': 1},
    'C': {'A': 1}
}
d = 0.85
print(page_rank(graph, d))  # should print {'A': 0.425, 'B': 0.225, 'C': 0.225}

