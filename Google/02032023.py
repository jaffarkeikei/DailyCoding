# Given pre-order and in-order traversals of a binary tree, write a function to reconstruct the tree.
#
# For example, given the following preorder traversal:
#
# [a, b, d, e, c, f, g]
#
# And the following inorder traversal:
#
# [d, b, e, a, f, c, g]
#
# You should return the following tree:
#
#     a
#    / \
#   b   c
#  / \ / \
# d  e f  g

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def buildTree(preorder, inorder):
    if not preorder or not inorder:
        return None

    root = TreeNode(preorder[0])
    root_index = inorder.index(preorder[0])
    left_inorder = inorder[:root_index]
    right_inorder = inorder[root_index + 1:]
    left_preorder = preorder[1:1 + len(left_inorder)]
    right_preorder = preorder[1 + len(left_inorder):]

    root.left = buildTree(left_preorder, left_inorder)
    root.right = buildTree(right_preorder, right_inorder)

    return root
