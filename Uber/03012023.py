# A rule looks like this:
#
# A NE B
#
# This means point A is located northeast of point B.
#
# A SW C
#
# means that point A is southwest of C.
#
# Given a list of rules, check if the sum of the rules validate. For example:
#
# A N B
#  NE C
#  N A
# does not validate, since A cannot be both north and south of C.
#
# A NW B
# A N B
# is considered valid.

def is_valid(rules):
    graph = {}
    for rule in rules:
        a, direction, b = rule.split()
        if a not in graph:
            graph[a] = set()
        graph[a].add(b)
    visited = set()
    for node in graph:
        if node in visited:
            continue
        stack = [node]
        while stack:
            current = stack.pop()
            if current in visited:
                return False
            visited.add(current)
            for neighbor in graph.get(current, []):
                stack.append(neighbor)
    return True

rules = [
    "A N B",
    "B NE C",
    "C N A"
]
print(is_valid(rules)) # False

rules = [
    "A NW B",
    "A N B"
]
print(is_valid(rules)) # True
