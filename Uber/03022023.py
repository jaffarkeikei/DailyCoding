# Write a program that determines the smallest number of perfect squares that sum up to N.
#
# Here are a few examples:
#
# Given N = 4, return 1 (4)
# Given N = 17, return 2 (16 + 1)
# Given N = 18, return 2 (9 + 9)

import math

def numSquares(n):
    if n <= 0:
        return 0
    squares = []
    i = 1
    while i * i <= n:
        squares.append(i * i)
        i += 1
    dp = [float('inf')] * (n + 1)
    dp[0] = 0
    for i in range(1, n + 1):
        for square in squares:
            if i < square:
                break
            dp[i] = min(dp[i], dp[i - square] + 1)
    return dp[n]
