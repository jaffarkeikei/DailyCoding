# Pascal's triangle is a triangular array of integers constructed with the following formula:
#
# The first row consists of the number 1.
# For each subsequent row, each element is the sum of the numbers directly above it, on either side.
# For example, here are the first few rows:
#
#     1
#    1 1
#   1 2 1
#  1 3 3 1
# 1 4 6 4 1

def generate_pascal_triangle(n):
    triangle = []
    for i in range(n):
        row = [1] * (i + 1)
        for j in range(1, i):
            row[j] = triangle[i-1][j-1] + triangle[i-1][j]
        triangle.append(row)
    return triangle
