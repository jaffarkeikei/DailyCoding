# You are given an array X of floating-point numbers x1, x2, ... xn. These can be rounded up or down to create a
# corresponding array Y of integers y1, y2, ... yn.
#
# Write an algorithm that finds an appropriate Y array with the following properties:
#
# The rounded sums of both arrays should be equal. The absolute pairwise difference between elements is minimized. In
# other words, |x1- y1| + |x2- y2| + ... + |xn- yn| should be as small as possible. For example, suppose your input
# is [1.3, 2.3, 4.4]. In this case you cannot do better than [1, 2, 5], which has an absolute difference of |1.3 - 1|
# + |2.3 - 2| + |4.4 - 5| = 1.

def minimize_absolute_difference(X):
    # Initialize the total_diff variable to 0
    total_diff = 0

    # Iterate over the elements of array X
    for x in X:
        # Calculate the rounded value of x as y
        y = round(x)
        # Add the absolute difference between x and y to total_diff
        total_diff += abs(x - y)

    # Return total_diff
    return total_diff


# Test the minimize_absolute_difference function
X = [1.3, 2.3, 4.4]
print(minimize_absolute_difference(X))  # should print 1
