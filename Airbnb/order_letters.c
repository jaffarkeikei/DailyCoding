/*You come across a dictionary of sorted words in a language 
you've never seen before. Write a program that returns the 
correct order of letters in this language.

For example, given ['xww', 'wxyz', 'wxyw', 'ywx', 'ywz'],
 you should return ['x', 'z', 'w', 'y']
 */

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#define ALPHABET_SIZE 26

void DFS(int graph[ALPHABET_SIZE][ALPHABET_SIZE], int visited[], int node, char* result, int* index) {
    visited[node] = 1;
    for (int i = 0; i < ALPHABET_SIZE; i++) {
        if (graph[node][i] && !visited[i]) {
            DFS(graph, visited, i, result, index);
        }
    }
    result[(*index)++] = node + 'a';
}

int main() {
    char* words[] = {"xww", "wxyz", "wxyw", "ywx", "ywz"};
    int num_words = sizeof(words) / sizeof(words[0]);
    int graph[ALPHABET_SIZE][ALPHABET_SIZE] = {0};
    int visited[ALPHABET_SIZE] = {0};
    char result[ALPHABET_SIZE] = {0};
    int index = 0;
    int exists[ALPHABET_SIZE] = {0}; // New array to keep track of existing characters

    for (int i = 0; i < num_words; i++) {
        for (int j = 0; j < strlen(words[i]); j++) {
            // Mark character as existing
            exists[words[i][j]-'a'] = 1;
            if (i > 0 && j < strlen(words[i-1]) && words[i-1][j] != words[i][j]) {
                graph[words[i-1][j]-'a'][words[i][j]-'a'] = 1;
                break;
            }
        }
    }

    for (int i = 0; i < ALPHABET_SIZE; i++) {
        if (exists[i] && !visited[i]) { // Only perform DFS on existing characters
            DFS(graph, visited, i, result, &index);
        }
    }

    for (int i = ALPHABET_SIZE - 1; i >= 0; i--) {
        if (result[i] != 0) {
            printf("%c ", result[i]);
        }
    }

    return 0;
}
