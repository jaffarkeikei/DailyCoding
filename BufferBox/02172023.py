# Given a binary tree where all nodes are either 0 or 1, prune the tree so that subtrees containing all 0s are removed.
#
# For example, given the following tree:
#
#    0
#   / \
#  1   0
#     / \
#    1   0
#   / \
#  0   0
# should be pruned to:
#
#    0
#   / \
#  1   0
#     /
#    1
# We do not remove the tree at the root or its left child because it still has a 1 as a descendant.

class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

def pruneTree(root: TreeNode) -> TreeNode:
    if not root:
        return None

    root.left = pruneTree(root.left)
    root.right = pruneTree(root.right)

    if not root.left and not root.right and root.val == 0:
        return None

    if not root.left and not root.right:
        return root

    if root.left and not root.left.left and not root.left.right and root.left.val == 0:
        root.left = None

    if root.right and not root.right.left and not root.right.right and root.right.val == 0:
        root.right = None

    return root
