# There exists a staircase with N steps, and you can climb up either 1 or 2 steps at a time. Given N,
# write a function that returns the number of unique ways you can climb the staircase. The order of the steps matters.
#
# For example, if N is 4, then there are 5 unique ways:
#
# 1, 1, 1, 1 2, 1, 1 1, 2, 1 1, 1, 2 2, 2 What if, instead of being able to climb 1 or 2 steps at a time,
# you could climb any number from a set of positive integers X? For example, if X = {1, 3, 5}, you could climb 1, 3,
# or 5 steps at a time.


def climb_staircase(N):
    # Initialize the ways list with the base case that there is only 1 way to climb 0 steps
    ways = [1]

    # Iterate over the range 1.N
    for i in range(1, N + 1):
        # Set ways[i] to the sum of ways[i - 1] and ways[i - 2]
        ways.append(ways[i - 1] + ways[i - 2])

    # Return ways[N]
    return ways[N]


# Test the climb_staircase function
N = 4
print(climb_staircase(N)) # should print 5


def climb_staircase(N, X):
    # Initialize the ways list with the base case that there is only 1 way to climb 0 steps
    ways = [1]

    # Iterate over the range 1.N
    for i in range(1, N + 1):
        # Set ways[i] to 0
        ways.append(0)
        # Iterate over the numbers in the set X
        for x in X:
            # If x is less than or equal to i, add ways[i - x] to ways[i]
            if x <= i:
                ways[i] += ways[i - x]

    # Return ways[N]
    return ways[N]


# Test the climb_staircase function
N = 4
X = {1, 3, 5}
print(climb_staircase(N, X))  # should print 3
