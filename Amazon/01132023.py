# For example, suppose you are given the input 3 -> 4 -> -7 -> 5 -> -6 -> 6.
# In this case, you should first remove 3 -> 4 -> -7, then -6 -> 6, leaving only 5.
class Node:
    def __init__(self, val):
        self.val = val
        self.next = None


def remove_negative_sum_sublists(head: Node) -> Node:
    dummy = Node(0)
    dummy.next = head
    curr = dummy
    sum = 0
    while curr.next:
        sum += curr.next.val
        if sum < 0:
            curr.next = curr.next.next
            sum = 0
        else:
            curr = curr.next
    return dummy.next


def print_list(head: Node):
    current = head
    while current:
        print(current.val, end=' ')
        current = current.next
    print()

# Print the remaining list
# print_list(head)
