# Implement a stack API using only a heap. A stack implements the following methods:
#
# push(item), which adds an element to the stack
# pop(), which removes and returns the most recently added element (or throws an error
# if there is nothing on the stack)
# Recall that a heap has the following operations:
#
# push(item), which adds a new key to the heap
# pop(), which removes and returns the max value of the heap

import time
import heapq


class Stack:
    def __init__(self):
        self.heap = []

    def push(self, item):
        heapq.heappush(self.heap, (time.time(), item))

    def pop(self):
        if not self.heap:
            raise Exception("Stack is empty")
        return heapq.heappop(self.heap)[1]
