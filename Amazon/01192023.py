# Given an array of a million integers between zero and a billion, out of order,
# how can you efficiently sort it? Assume that you cannot store an array of a
# billion elements in memory.in

# One way to efficiently sort a large array of integers that cannot fit entirely
# in memory is to use an external sorting algorithm such as the "External Merge Sort"
# algorithm. This algorithm works by dividing the large array into smaller chunks that
# can fit in memory, then sorting each chunk individually using an internal sorting a
# lgorithm like quicksort or mergesort. Once all the chunks are sorted, the algorithm
# repeatedly merges the chunks together in a sorted order until the entire array is sorted.
# This method takes advantage of the fact that sorting small chunks of data is much faster
# than sorting the entire large array at once, and allows the algorithm to work with the
# data in chunks that can fit in memory.

import os
import heapq

def external_sort(file_path):
    # Define the chunk size
    chunk_size = 1000000
    # Define the temporary file name
    temp_file_name = "temp_file.txt"

    # Read the input file in chunks and sort them
    with open(file_path, 'r') as f:
        chunks = [sorted(list(map(int, chunk.split()))) for chunk in iter(lambda: f.readline(), '')][:chunk_size]
    # Open a temporary file for writing
    with open(temp_file_name, 'w') as f:
        # Write each sorted chunk to the temporary file
        for chunk in chunks:
            for number in chunk:
                f.write(str(number) + '\n')
    # Open the temporary file for reading
    with open(temp_file_name, 'r') as f:
        # Merge the chunks
        result = list(heapq.merge(*(list(map(int, chunk.split())) for chunk in iter(lambda: f.readline(), ''))))
    # Write the result to output file
    with open(file_path, 'w') as f:
        for number in result:
            f.write(str(number) + '\n')
    # Remove the temporary file
    os.remove(temp_file_name)

# Use the function
external_sort("large_file.txt")
