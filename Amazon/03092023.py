# A tree is symmetric if its data and shape remain unchanged when it is reflected
# about the root node. The following tree is an example:
#
#         4
#       / | \
#     3   5   3
#   /           \
# 9              9
# Given a k-ary tree, determine whether it is symmetric.
class Node:
    def __init__(self, value, children=[]):
        self.value = value
        self.children = children

def is_symmetric(root):
    def is_mirror(t1, t2):
        if t1 is None and t2 is None:
            return True
        if t1 is None or t2 is None:
            return False
        if t1.value != t2.value:
            return False
        return all(is_mirror(t1.children[i], t2.children[len(t2.children) - 1 - i]) for i in range(len(t1.children)))

    return is_mirror(root, root)
