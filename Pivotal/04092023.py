# Write an algorithm that finds the total number of
# set bits in all integers between 1 and N.

def count_set_bits(N):
    count = 0  # Initialize a variable to keep track of the total number of set bits
    while N > 0:  # While N is greater than 0
        count += bin(N).count('1')  # Add the number of set bits in the binary representation of N to the count
        N >>= 1  # Right shift N by 1 bit to remove the least significant bit
    return count  # Return the total number of set bits

