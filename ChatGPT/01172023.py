# Write a Python function that takes a list of integers as input, and returns the kth largest element using a min
# heap data structure.

import heapq

def kth_largest(nums, k):
    min_heap = []
    for num in nums:
        heapq.heappush(min_heap, num)
        if len(min_heap) > k:
            heapq.heappop(min_heap)
    return min_heap[0]
