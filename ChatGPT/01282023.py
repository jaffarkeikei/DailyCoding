# Given a matrix of integers, find the length of the longest increasing submatrix.
#
# less
from typing import List


def longest_increasing_submatrix(matrix: List[List[int]]) -> int:
    if not matrix or not matrix[0]:
        return 0
    m, n = len(matrix), len(matrix[0])
    dp = [0] * (m*n)
    max_length = 0
    for i in range(m):
        for j in range(n):
            dp[i*n + j] = matrix[i][j]
    max_length = LIS(dp)
    return max_length

def LIS(arr:List[int]) -> int:
    dp = [0]*len(arr)
    dp[0] = arr[0]
    max_length = 1
    for i in range(1, len(arr)):
        if arr[i] > dp[max_length - 1]:
            dp[max_length] = arr[i]
            max_length += 1
        else:
            index = binary_search(dp, 0, max_length - 1, arr[i])
            dp[index] = arr[i]
    return max_length

def binary_search(arr:List[int], low:int, high:int, key:int)->int:
    while low <= high:
        mid = low + (high - low)//2
        if arr[mid] == key:
            return mid
        elif arr[mid] > key:
            high = mid - 1
        else:
            low = mid + 1
    return low

