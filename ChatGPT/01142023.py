# Write a function in Python that takes in two heaps represented as lists, and returns a new heap that is the
# result of merging the two input heaps. The function should maintain the heap property (i.e. the parent no
# des should be smaller than the child nodes).

import heapq

def merge_heaps(heap1, heap2):
    # combine the two heaps into one list
    combined_heap = heap1 + heap2
    # use the heapify function to convert the list into a heap
    heapq.heapify(combined_heap)
    return combined_heap
