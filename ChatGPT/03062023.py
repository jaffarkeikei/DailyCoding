# dijkstra's shortest path

import heapq
from collections import defaultdict


def dijkstra(graph, start):
    heap = [(0, start)]
    visited = set()
    distances = defaultdict(lambda: float('inf'))
    distances[start] = 0

    while heap:
        (distance, current) = heapq.heappop(heap)
        if current in visited:
            continue
        visited.add(current)

        for neighbor, weight in graph[current].items():
            new_distance = distance + weight
            if new_distance < distances[neighbor]:
                distances[neighbor] = new_distance
                heapq.heappush(heap, (new_distance, neighbor))

    return distances
