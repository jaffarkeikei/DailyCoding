# This problem was asked by Microsoft.
#
# Let's represent an integer in a linked list format by having each
# node represent a digit in the number. The nodes make up the number in reversed order.
#
# For example, the following linked list:
#
# 1 -> 2 -> 3 -> 4 -> 5
# is the number 54321.
#
# Given two linked lists in this format, return their sum in the same linked list format.
#
# For example, given
#
# 9 -> 9
# 5 -> 2
# return 124 (99 + 25) as:
#
# 4 -> 2 -> 1
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def addTwoNumbers(l1: ListNode, l2: ListNode) -> ListNode:
    carry = 0
    dummy = ListNode(0)
    curr = dummy
    while l1 or l2:
        x = l1.val if l1 else 0
        y = l2.val if l2 else 0
        sum = x + y + carry
        carry = sum // 10
        curr.next = ListNode(sum % 10)
        curr = curr.next
        if l1: l1 = l1.next
        if l2: l2 = l2.next
    if carry > 0:
        curr.next = ListNode(carry)
    return dummy.next
