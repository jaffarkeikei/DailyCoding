# Implement 3 stacks using a single list:

class Stack:
    def __init__(self):
        self.list = []
        self.stack_pointers = [-3, -2, -1]

    def pop(self, stack_number):
        index = self.stack_pointers[stack_number - 1]
        if index >= 0:
            self.stack_pointers[stack_number - 1] -= 3
            return self.list.pop(index)
        else:
            return None

    def push(self, item, stack_number):
        self.stack_pointers[stack_number - 1] += 3
        self.list.append(item)

# This solution uses the pop method of the list to remove
# items, which is an O(1) operation. Additionally, each stack
# uses an offset of 3 * n to calculate its index, where n is
# the number of items in the stack. This ensures that the items
# in each stack are separated by a fixed distance in the list,
# making the push and pop operations more efficient.

