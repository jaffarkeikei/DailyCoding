# You have an N by N board. Write a function that, given N, returns the number of possible arrangements of the board
# where N queens can be placed on the board without threatening each other, i.e. no two queens share the same row,
# column, or diagonal.
#
# def count_n_queens(n):
#     def is_valid(board, row, col):
#         for i in range(row):
#             if board[i] == col or abs(board[i] - col) == abs(i - row):
#                 return False
#         return True
#
#     def backtrack(board, row):
#         if row == n:
#             return 1
#
#         count = 0
#         for col in range(n):
#             if is_valid(board, row, col):
#                 board[row] = col
#                 count += backtrack(board, row + 1)
#                 board[row] = -1
#         return count
#
#     board = [-1 for _ in range(n)]
#     return backtrack(board, 0)


def count_n_queens(n):
    def backtrack(ld, col, rd, count):
        if col == (1 << n) - 1:
            count[0] += 1
            return

        pos = (~(ld | col | rd)) & ((1 << n) - 1)
        while pos:
            p = pos & -pos
            pos -= p
            backtrack((ld | p) << 1, col | p, (rd | p) >> 1, count)

    count = [0]
    backtrack(0, 0, 0, count)
    return count[0]
