# A number is considered perfect if its digits sum up to exactly 10.
#
# Given a positive integer n, return the n-th perfect number.
#
# For example, given 1, you should return 19. Given 2, you should return 28.

n = int(input("Enter the value of n: "))

count = 0
i = 19
while count < n:
    temp = i
    digits_sum = sum(int(d) for d in str(temp))
    if digits_sum == 10:
        count += 1
    if count == n:
        print("The {}-th perfect number is {}".format(n, i))
        break
    i += 1


