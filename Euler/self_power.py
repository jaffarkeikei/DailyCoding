def self_power(m, n):
    total = 0
    for i in range(n-m):
        total += (m+i)**(m+i)
    return total

m = 1
n = 1000
print(self_power(m, n))