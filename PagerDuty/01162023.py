# Given a positive integer N, find the smallest number of steps it will take to reach 1.
# There are two kinds of permitted steps: You may decrement N to N - 1.
# If a * b = N, you may decrement N to the large of a and b.
# For example, given 100, you can reach 1 in five steps with the following route: 100 -> 10 -> 9 -> 3 -> 2 -> 1.

from queue import Queue


def minimum_steps(N):
    q = Queue()
    q.put(N)
    steps = 0
    while not q.empty():
        current = q.get()
        if current == 1:
            return steps
        steps += 1
        q.put(current - 1)
        for i in range(2, int(current ** 0.5) + 1):
            if current % i == 0:
                q.put(max(i, current // i))
