# The "look and say" sequence is defined as follows: beginning with the term 1, each subsequent term visually
# describes the digits appearing in the previous term. The first few terms are as follows:
#
# 1
# 11
# 21
# 1211
# 111221
# As an example, the fourth term is 1211, since the third term consists of one 2 and one 1.
#
# Given an integer N, print the Nth term of this sequence.


def look_and_say(string):
    # Initialize the result variable to an empty string
    result = ""
    # Initialize the count variable to 1
    count = 1

    # Iterate over the characters in the input string
    for i in range(1, len(string)):
        # Check if the character at index i is equal to the previous character
        if string[i] == string[i - 1]:
            # If it is, increment count by 1
            count += 1
        else:
            # If it is not, append the value of count and the character to result, and reset count to 1
            result += str(count) + string[i - 1]
            count = 1

    # Append the value of count and the last character to result
    result += str(count) + string[-1]

    # Return result
    return result


# Test the look_and_say function
string = "1112212"
print(look_and_say(string))  # should print "312211"
